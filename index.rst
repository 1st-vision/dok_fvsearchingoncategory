.. |label| replace:: Suche nur auf Kategorieebene
.. |snippet| replace:: FvSearchingOnCategory
.. |Author| replace:: 1st Vision GmbH
.. |Entwickler| replace:: Internetfabrik
.. |minVersion| replace:: 5.4.0
.. |maxVersion| replace:: 5.5.x
.. |Version| replace:: 0.0.1
.. |php| replace:: 7.0


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis


Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |Version|

Voraussetzung
-------------
Kein Einsatz des Plugins "Intelligente Suche"

Beschreibung
------------
Suche nur auf Kategorieebene. Egal was man sucht auch wenn es die Artikelnummer ist, werden immer als Suchergebnis die Kategorien angezeigt; der Besucher muss dann die jeweilige Kategorie anklicken um dorthin zu gelangen.

Wenn nur eine Kategorie gefunden wird, dann erfolgt eine direkte Weiterleitung dorthin.

Nach Anklicken des Links zur Kategorie werden die gesuchten Produkte farblich hervorgehoben.

Frontend
--------
Erweiterung Autosuggest:

.. image:: frontend1.png


Erweiterung Suchergebnisseite (nach Abschicken mit „Return“):

.. image:: frontend2.png


Backend
-------
Keine Backend-Erweiterung vorhanden.


technische Beschreibung
------------------------

Shop-Datenbank:
_______________
Keine zusätzliche Tabelle vorhanden.



Modifizierte Template-Dateien
-----------------------------
- search/ajax.tpl
- search/fuzzy.tpl
